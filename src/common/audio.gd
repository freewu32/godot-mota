extends Node

## 音频
class_name Audio

@onready var bgm: AudioStreamPlayer = $"BGM"

@onready var sfx: AudioStreamPlayer = $"SFX"

## 背景音文件路径
@export var bgm_path : String = "res://assest/bgm"
## 音效文件路径
@export var sfx_path : String = "res://assest/sfx"

## 播放音效
func play_sfx(stream: AudioStream, volume_db = 0):
	self.sfx.stream = stream
	if volume_db != 0:
		self.sfx.volume_db = volume_db
	self.sfx.play()
