extends Item

## 楼梯
class_name Stairs

## 变更的楼层
@export var change_level: int = 1

# 玩家触碰
func _on_player_collide(player: Player):
	var hud = $"/root/LevelManager/HUD" as HUD
	if change_level > 0:
		hud.show_toast("走上了楼梯")
	else:
		hud.show_toast("走下了楼梯")
	if sfx != null:
		_audio.play_sfx(sfx)
	var level_manager = $"/root/LevelManager"
	level_manager.set_level(level_manager.get_level() + change_level)
	return true
