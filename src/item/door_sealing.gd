extends Door

## 封印门
class_name  SealingDoor

## 封印门守护怪物
@export var monsters: Array[Monster] = []

## 已经死亡的怪物数量
var _dead_count = 0

func _ready():
	for monster in monsters:
		monster.connect("dead", _on_monster_dead.bind())

## 玩家触碰
func _on_player_collide(player: Player):
	return false

## 怪物死亡监听时
func _on_monster_dead():
	self._dead_count += 1
	if self._dead_count == monsters.size():
		self.open()

func get_data() -> Dictionary:
	return {
		"dead_count": _dead_count
	}

func set_data(data: Dictionary):
	self._dead_count = data["dead_count"]
