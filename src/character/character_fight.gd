extends RefCounted

## 角色战斗类
class_name CharacterFight

## 角色1，发起攻击方
var _character1: Character

## 角色2，接收攻击方
var _character2: Character

## 初始化
func _init(character1:Character, character2:Character):
	self._character1 = character1
	self._character2 = character2

## 开始战斗
func fight() -> int:
	while true:
		# 角色1攻击角色2
		var damage1 = await self._character1.attack(self._character2)
		if damage1 <= 0:
			return -1
		if self._character2.hp - damage1 <= 0:
			self._character2.hp = 0
			self._character1.money += self._character2.money
			return 1
		else:
			self._character2.hp -= damage1
		# 角色2攻击角色1
		var damage2 = await self._character2.attack(self._character1)
		if damage2 < 0:
			damage2 = 0
		if self._character1.hp - damage2 <= 0:
			self._character1.hp = 0
			self._character2.money += self._character1.money
			return 0
		else:
			self._character1.hp -= damage2
	return -1
