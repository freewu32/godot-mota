extends NPC

## 卖出物品的NPC
class_name SellingNPC

## 需要显示的对话
@export var selling_message : String = ""
## 卖完货后提示的内容
@export var prompt : String = ""
## 购买需要的金币
@export var require_money : int = 0
## 购买后修改用户的属性名
@export var update_property: String = ""
## 购买后修改用户属性的数量
@export var update_property_value: int = 0


## 是否已经卖出
var _sold = false

## 获取npc对话
func _get_dialogue() -> String:
	if self._sold:
		return self.prompt
	else:
		return self.selling_message

## 获取npc可以执行的行为
func _get_actions() -> Array:
	if self._sold:
		return [
			{
				"text": "好的",
				"pressed": _on_action3.bind()
			}
		]
	else:
		return [
	{
		"text": "我太需要了",
		"pressed": _on_action1.bind()
	},
	{
		"text": "下次再说",
		"pressed": _on_action2.bind()
	}
	]

## 点击购买的逻辑
func _on_action1():
	var player = $"/root/LevelManager/Player" as Player
	if player.money < self.require_money:
		hud.show_toast("你没有那么多钱")
		hud.hidde_npc_dialog()
		return
	player.money -= self.require_money
	player[self.update_property] += self.update_property_value
	self._sold = true
	hud.hidde_npc_dialog()
	
## 点击下次再说的逻辑
func _on_action2():
	hud.hidde_npc_dialog()

## 点击收下后的逻辑
func _on_action3():
	hud.hidde_npc_dialog()
	var tween = create_tween()
	tween.tween_property(self, "modulate", Color(1.0,1.0,1.0,0.0), 0.5)
	tween.tween_callback(self.queue_free)
