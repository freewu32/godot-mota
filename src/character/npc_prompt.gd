extends NPC

## 提示npc
class_name PromptNPC

## 需要提示的内容
@export var prompt : String = ""

## 获取npc对话
func _get_dialogue() -> String:
	return prompt

## 获取npc可以执行的行为
func _get_actions() -> Array:
	return [{
		"text": "好的",
		"pressed": _on_action1.bind()
	}]

## 点击收下后的逻辑
func _on_action1():
	hud.hidde_npc_dialog()
	var tween = create_tween()
	tween.tween_property(self, "modulate", Color(1.0,1.0,1.0,0.0), 0.5)
	tween.tween_callback(self.queue_free)
