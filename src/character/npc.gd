extends Character

## NPC
class_name NPC

## 玩家触碰开始对话
func _on_player_collide(player: Player):
	hud.show_npc_dialog(self.character_name, self._get_dialogue(), self._get_actions(), false)

## 获取npc对话
func _get_dialogue() -> String:
	return ""

## 获取npc可以执行的行为
func _get_actions() -> Array:
	return []
