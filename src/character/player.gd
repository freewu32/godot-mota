extends Character

## 玩家类
class_name Player

@onready var _sprite: Sprite2D = $"Sprite2D"

var _footstep = preload("res://assest/sfx/character/footstep06.ogg")

## 黄钥匙
var yellow_key: int = 0:
	set(value):
		yellow_key = value
		emit_signal("yellow_key_changed", value)
## 蓝钥匙
var blue_key: int = 0:
	set(value):
		blue_key = value
		emit_signal("blue_key_changed", value)
## 红钥匙
var red_key: int = 0:
	set(value):
		red_key = value
		emit_signal("red_key_changed", value)

## 持有的道具
var props: Array = []

## 黄色钥匙数量变更通知
signal yellow_key_changed(count)
## 蓝色钥匙数量变更通知
signal blue_key_changed(count)
## 红色钥匙数量变更通知
signal red_key_changed(count)
## 道具增加通知
signal prop_added(prop)

## 增加道具
func add_prop(prop: String):
	if self.props.has(prop):
		return
	self.props.append(prop)
	self.emit_signal("prop_added", prop)

## 读取玩家数据
func get_data() -> Dictionary:
	return {
		"character_name": character_name,
		"hp": hp,
		"atk": atk,
		"def": def,
		"money": money,
		"key": [yellow_key,blue_key,red_key],
		"props": self.props,
		"position": [self.position.x,self.position.y]
	}

## 设置玩家数据
func set_data(data: Dictionary):
	self.character_name = data["character_name"]
	self.hp = data["hp"]
	self.atk = data["atk"]
	self.def = data["def"]
	self.money = data["money"]
	self.yellow_key = data["key"][0]
	self.blue_key = data["key"][1]
	self.red_key = data["key"][2]
	for prop in data["props"]:
		self.add_prop(prop)
	self.position = Vector2(data["position"][0],data["position"][1])

## 玩家输入控制
func _unhandled_input(event):
	if event.is_action_pressed("ui_left", true):
		self.move_left()
		return
	if event.is_action_pressed("ui_right", true):
		self.move_right()
		return
	if event.is_action_pressed("ui_up", true):
		self.move_up()
		return
	if event.is_action_pressed("ui_down", true):
		self.move_down()
		return
	if event is InputEventMouseButton and event.is_pressed():
		self._navigation_to(event.position)
		return

## 导航到指定位置
func _navigation_to(pos):
	var level_map = $"/root/LevelManager".get_current_level_node() as LevelMap
	var paths = level_map.get_route_path(self.position, pos)
	for index in paths.size():
		if index == 0:
			continue
		var path = paths[index]
		var result = await self.move_to(path - self.position)
		if not result:
			break
		await get_tree().create_timer(0.2).timeout
		
## 玩家碰撞道具、建筑物检测
func _on_move(pos: Vector2):
	self._move_feedback()
	var collider = _get_colldier(pos)
	if collider == null:
		return true
	if collider.has_method("_on_player_collide") && \
		collider.get_position().distance_to(self.position) != 0:
		return collider.call("_on_player_collide", self)
	if collider is Node2D:
		var collider_parent = collider.get_parent() as Node2D
		if collider_parent != null && collider_parent.has_method("_on_player_collide"):
			return await collider_parent.call("_on_player_collide", self)
	return false

## 移动反馈
func _move_feedback():
	if self._sprite.frame + 1 >= self._sprite.hframes:
		self._sprite.frame = 0
	else:
		self._sprite.frame += 1
	self._audio.play_sfx(_footstep, -15)

## 获取被碰撞的对象
func _get_colldier(pos: Vector2):
	var space_state = get_world_2d().direct_space_state
	var query = PhysicsPointQueryParameters2D.new()
	query.position = self.position + pos
	var result = space_state.intersect_point(query)
	var max_distance = 0
	var max_colldier = null
	for result_item in result:
		var collider = result_item.collider
		if not collider.is_visible_in_tree():
			continue
		if collider.position.distance_to(self.position) > max_distance:
			max_colldier = collider
	return max_colldier

## 主角死亡处理
func _on_dead():
	hud.show_npc_dialog("死亡", "你死了", [], true)
